#Author-Dag Rende
#Description-Add-In helper object - create add-ins without all boiler-plate code

from collections import defaultdict

import adsk.core, adsk.fusion
import math
import traceback
import re
import os

# pip install pyyaml -t packages; mv packages/yaml .; rm -rf packages
from . import yaml

import time
from . import utils

# Input a selection of type "type".
# maxInputs = 0 means unlimited number of selections.
class SelectionParam:
    def __init__(self, name, types, title = '', tooltip = '', maxInputs = 0):
        self.name = name
        self.types = types
        self.title = title
        self.tooltip = tooltip
        self.maxInputs = maxInputs
    def addInput(self, inputs, defaults, design):
        self.theInput = inputs.addSelectionInput(self.name, self.title, self.tooltip)
        self.theInput.addSelectionFilter(self.types)
        self.theInput.setSelectionLimits(1, self.maxInputs)
    def parseAttribute(self, inputs, attributes, defaults):
        attributes[self.name] = [self.theInput.selection(i).entity for i in range(self.theInput.selectionCount)]

class StringParam:
    def __init__(self, name, defaultValue = '', title = '', tooltip = ''):
        self.name = name
        self.defaultValue = defaultValue
        self.title = title
        self.tooltip = tooltip
    def addInput(self, inputs, defaults, design):
        self.theInput = inputs.addStringValueInput(
            self.name, self.title,
            defaults[self.name])
        self.theInput.tooltip = self.tooltip
    def parseAttribute(self, inputs, attributes, defaults):
        attributes[self.name] = self.theInput.value
        defaults[self.name] = self.theInput.value

class BoolParam:
    def __init__(self, name, defaultValue = 'false', title = '', tooltip = ''):
        self.name = name
        self.defaultValue = defaultValue
        self.title = title
        self.tooltip = tooltip
    def addInput(self, inputs, defaults, design):
        self.theInput = inputs.addBoolValueInput(
            self.name, self.title, True, "", defaults[self.name])
        self.theInput.tooltip = self.tooltip
    def parseAttribute(self, inputs, attributes, defaults):
        attributes[self.name] = self.theInput.value
        defaults[self.name] = self.theInput.value

class NumberParam:
    def __init__(self, name, unit = '', defaultValue = '0', title = '', tooltip = ''):
        self.name = name
        self.unit = unit
        self.defaultValue = defaultValue
        self.title = title
        self.tooltip = tooltip
    def addInput(self, inputs, defaults, design):
        self.theInput = inputs.addValueInput(
            self.name, self.title, self.unit,
            adsk.core.ValueInput.createByString(defaults[self.name]))
        self.theInput.tooltip = self.tooltip
    def parseAttribute(self, inputs, attributes, defaults):
        attributes[self.name] = self.theInput.value
        defaults[self.name] = self.theInput.expression

class AddIn(object):
    COMMAND_ID = "wingProfileImporter"

    def __init__(self, commandId, commandName, params):
        try:
            self.COMMAND_ID = commandId
            self.commandName = commandName
            # app specific param descriptions
            self.params = params
            # set from defaults.yml at start and on dialog submit
            self.defaults = {}
            # parsed params from dialog on submit
            self.attributes = {}
            self.description = ""
            self.resourcePath = 'Resources'
            self.addDialogFields = None
            self.parseDialogFields = None
            self.fieldValidator = None
            self.executor = None

            self.app = adsk.core.Application.get()
            self.ui = self.app.userInterface

            self.handlers = utils.HandlerHelper()
            self.appPath = os.path.dirname(os.path.abspath(__file__))

            for param in self.params:
                if hasattr(param, 'defaultValue'):
                    self.defaults[param.name] = param.defaultValue
        except:
            utils.messageBox(traceback.format_exc())

    def writeDefaults(self):
        try:
            with open(os.path.join(self.appPath, 'defaults.yml'), 'w') as file:
                documents = yaml.dump(self.defaults, file)
        except Exception as e:
            log('writeDefaults error: ' + str(e))
    
    def readDefaults(self): 
        if not os.path.isfile(os.path.join(self.appPath, 'defaults.yml')):
            return
        with open(os.path.join(self.appPath, 'defaults.yml'), 'r') as file:
            self.defaults.update(yaml.safe_load(file))

    def addButton(self):
        # clean up any crashed instances of the button if existing
        try:
            self.removeButton()
            # add add-in to UI
            log('addButton COMMAND_ID', self.COMMAND_ID)
            buttonDogbone = self.ui.commandDefinitions.addButtonDefinition(
                self.COMMAND_ID, self.commandName, self.description, self.resourcePath)

            buttonDogbone.commandCreated.add(self.handlers.make_handler(adsk.core.CommandCreatedEventHandler,
                                                                        self.onCreate))

            createPanel = self.ui.allToolbarPanels.itemById('SolidCreatePanel')
            buttonControl = createPanel.controls.addCommand(buttonDogbone, self.COMMAND_ID)

            # Make the button available in the panel.
            buttonControl.isPromotedByDefault = True
            buttonControl.isPromoted = True
        except Exception as e:
            log('addButton error: ' + str(e))


    def removeButton(self):
        try:
            cmdDef = self.ui.commandDefinitions.itemById(self.COMMAND_ID)
            if cmdDef:
                cmdDef.deleteMe()
            createPanel = self.ui.allToolbarPanels.itemById('SolidCreatePanel')
            cntrl = createPanel.controls.itemById(self.COMMAND_ID)
            if cntrl:
                cntrl.deleteMe()
        except Exception as e:
            log('removeButton error: ' + str(e))

    def onCreate(self, args):
        try:
            self.readDefaults()

            inputs = args.command.commandInputs
            args.command.setDialogInitialSize(425, 475)
            args.command.setDialogMinimumSize(425, 475)

            for param in self.params:
                param.addInput(inputs, self.defaults, self.design)

            # Add handlers to this command.
            args.command.execute.add(self.handlers.make_handler(adsk.core.CommandEventHandler, self.onExecute))
            args.command.validateInputs.add(
                self.handlers.make_handler(adsk.core.ValidateInputsEventHandler, self.onValidate))
            args.command.inputChanged.add(
                self.handlers.make_handler(adsk.core.InputChangedEventHandler, self.onInputChanged))
        except Exception as e:
            log('onCreate error: ' + str(e))

    def parseInputs(self, inputs):
        inputs = {inp.id: inp for inp in inputs}
        for param in self.params:
            param.parseAttribute(inputs, self.attributes, self.defaults)

    def onValidate(self, args):
        if self.fieldValidator:
            self.fieldValidator(self, args)

    def onExecute(self, args):
        app = adsk.core.Application.get()
        start = time.time()
        doc = app.activeDocument  
        design = app.activeProduct
        timeLine = design.timeline
        timeLineGroups = timeLine.timelineGroups
        timelineCurrentIndex = timeLine.markerPosition
        
        self.parseInputs(args.firingEvent.sender.commandInputs)
        self.writeDefaults()
        if self.executor:
            self.executor(self)
        
        timelineEndIndex = timeLine.markerPosition
        if timelineEndIndex - timelineCurrentIndex > 1:
            exportTimelineGroup = timeLineGroups.add(timelineCurrentIndex, timelineEndIndex-1)# the minus 1 thing works, weird.
            exportTimelineGroup.name = self.commandName
        
    def onInputChanged(self, args):
        cmd = args.firingEvent.sender

    @property
    def design(self):
        return self.app.activeProduct

    @property
    def rootComp(self):
        return self.design.rootComponent

    def log(self, *args):
        try:
            with open(os.path.join(self.appPath, 'log.txt'), 'a+') as file:
                print(*args, file=file)
        except:
            pass

def log(self, *args):
    try:
        with open(os.path.join(self.appPath, 'log.txt'), 'a+') as file:
            print(*args, file=file)
    except:
        pass

