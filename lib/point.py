import math

class Point:
    """ Point class for representing and manipulating x,y,z coordinates. """

    def __init__(self, x=0, y=0, z=0):
        """ Create a new point at x, y, z """
        self.x = x
        self.y = y
        self.z = z

    def __repr__(self):
        return f"Point({self.x}, {self.y}, {self.z})"

    def cross(self, p):
        return self.x * p.y - self.y * p.x
    
    def length(self):
        return math.sqrt(self.x * self.x + self.y * self.y)

    def normalize(self):
        l = self.length()
        self.x /= l
        self.y /= l
        return self
    
    def rotate90(self):
        self.x, self.y = -self.y, self.x
        return self

    def copy(self):
        return Point(self.x, self.y)

    def scaleBy(self, m):
        self.x *= m
        self.y *= m
        return self

    def add(self, p):
        self.x += p.x
        self.y += p.y
        return self

    def subtract(self, p):
        self.x -= p.x
        self.y -= p.y
        return self
