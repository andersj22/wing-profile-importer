import re, sys
# from point import Point
from . import point

def log(x):
    # return None
    print(x, file=sys.stderr)

class AirFoil:
    # read file into a new AirFoil instance
    # file can be of two formats:
    # Simple: title row followed by row with x y pars of floats, each between 0 and 1
    # Lenicer: title row followed by two integer numbers denoting the number of upper and lower side coords
    #   row with pairs for upper side followed by rows for lower side - both sides beginning in front end of wing
    # in both cases blank lines are ignored
    def __init__(self, filePath):
        # points from tail (x=1, y=0) along top side to front (x=0) and along bottom side back to tail (x=0)
        self.points = []
        with open(filePath, 'r') as file:
            self.title = file.readline().strip()
            log(f"title={self.title}")
            pair = self.readPair(file)
            log(f"first pair={pair}")
            if abs(pair.x) < 1.1:
                # Simple format
                log("format=Simple")
                while pair:
                    self.points.append(pair)
                    pair = self.readPair(file)
            else:
                # Lenicer format
                log("format=Lenicer")
                upperLowerCount = [pair.x, pair.y]

                # read upper side points
                pair = self.readPair(file)
                log(f"{pair}")
                while pair and len(self.points) < upperLowerCount[0]:
                    self.points.append(pair)
                    pair = self.readPair(file)
                    log(f"{pair}")
                if len(self.points) != upperLowerCount[0]:
                    # addIn.log(f"wrong nbr of upper points: {len(self.points)} - should have been {upperLowerCount[0]}")
                    raise RuntimeError(f"wrong nbr of upper points: {len(self.points)} - should have been {upperLowerCount[0]}")
                self.points.reverse()

                # read lower side points
                lowerPoints = []
                # addIn.log(f"first lower pair={pair}")
                while pair:
                    lowerPoints.append(pair)
                    pair = self.readPair(file)
                if len(lowerPoints) != upperLowerCount[1]:
                    # addIn.log(f"wrong nbr of lower points: {len(lowerPoints)} - should have been {upperLowerCount[1]}")
                    raise RuntimeError(f"wrong nbr of lower points: {len(lowerPoints)} - should have been {upperLowerCount[1]}")
                # addIn.log(f"lowerPoints={lowerPoints}")

                self.points = self.points + lowerPoints
                # addIn.log("end lower pairs")
            self.coordRange = self.calculateCoordRange(self.points)

    # returns next point.Point x, y read from file
    # ignores empty lines
    # treats nn. as nn
    # returns None if end of file
    def readPair(self, file):
        line = file.readline()
        # addIn.log(f"readPair line={line}")
        if not line:
            return None
        while len(line.strip()) == 0:
            line = file.readline()
            # addIn.log(f"readPair while line={line}")
            if not line:
                return None
        # remove decimal points without decimals - ex change "49. " to "49 "
        line = re.sub("\\.([^0-9]|$)", "\\g<1>", line, 0)
        # addIn.log(f"readPair line={line}")
        pair = line.strip().split()
        return point.Point(float(pair[0]), float(pair[1]))

    # returns Dict with min x,y point and max x,y point
    def calculateCoordRange(self, points):
        rangeMin = point.Point(sys.float_info.max, sys.float_info.max)
        rangeMax = point.Point(sys.float_info.min, sys.float_info.min)
        for p in points:
            rangeMin.x = min(rangeMin.x, p.x)
            rangeMin.y = min(rangeMin.y, p.y)
            rangeMax.x = max(rangeMax.x, p.x)
            rangeMax.y = max(rangeMax.y, p.y)
        return {"min": rangeMin, "max": rangeMax}
