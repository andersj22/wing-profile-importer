# WingProfileImporter

This [Fusion 360 add-in](#Install the command in Fusion 360) imports an airfoil profile and fits it to a predefined chord line.

The airfoil must be in the **.dat file-format**. Airfoils can be found here: 

http://airfoiltools.com

At airfoiltools you can also find NACA 4,5 and 6 digit profiles and generate .dat files from them.

You need to know about the concepts of **chord line** and **leading edge**. Learn more at https://en.wikipedia.org/wiki/Airfoil

Basically, all you need to know is that the "chord line" is a straight line from the "leading edge" to the "trailing edge" of the wing profile.

<img src="docs/Wing_profile_nomenclature.png" width="600"/>

## Import a profile into Fusion 360

After [installing](#Install the command in Fusion 360) the plugin you can import a wing profile like this:

1. Download a .dat file to import
2. Create a sketch In Fusion 360.
3. Draw a line representing the chord line of the airfoil profile. 
4. Run the WingProfileImporter command:

![WingProfileImporterCommand](docs/WingProfileImporterCommand.png)



* Click the WingProfileImporter command icon 
* Choose the chord line
* Choose the leading edge point
* Enter the path to the .dat file 
* Press OK and voilá, a new sketch with the airfoil profile is created

<img src="docs/ProfileSuccess.png" width="350"/>

## Install the command in Fusion 360

#### Download the code

Open https://gitlab.com/andersj22/wing-profile-importer in a web browser.

Click the download button: 
![gitlab download button](docs/gitlab-download.png)

Choose to download as a zip file. It will probably be immediately downloaded into your downloads directory.

Unpack the zip file somewhere where your fusion installation can find it.

#### Install the command in Fusion 360

Start Fusion 360
<img src="docs/HowToInstall.png" width="80000"/>

1.  Click the UTILITIES tab.
2. Click the ADD-INS button and choose "Scripts and Add-Ins..."
3. Click the Add-ins tab
4. Click the plus sign to add a command
5. Choose the wing-profile-importer directory
6. Click Open
7. Then click Run. Done!

You can find the command under the SOLID tab in the main Fusion 360 window and you should see a new tool-bar button 
![wing button](docs/wing-icon.png)
If the new button is not visible, you may widen the SOLID design toolbar by making the Fusion 360 window wider or by hiding the Data Panel. 