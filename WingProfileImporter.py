#Author-Dag Rende, Anders Jönsson
#Description-Import a Wing Profile/Airfoil into a Fusion360 sketch


import os, sys
from collections import defaultdict

import adsk.core, adsk.fusion
import traceback

from . import utils
from . import addin
from .lib import airfoil

def fieldValidator(addIn, args):
    cmd = args.firingEvent.sender

    # define attribute validation (part 4 of 5)
    for input in cmd.commandInputs:
        if input.id == 'airfoilPath':
            if input.value and len(input.value) > 0:
                if not os.path.isfile(input.value):
                    args.areInputsValid = False
            else:
                args.areInputsValid = False

def inputChanged(addIn, args):
    pass

# create points for the foils on two planes that are rootTipDistance apart
# create a loft between the two foils to visualize the wing
def executor(addIn):
    if not addIn.design:
        raise RuntimeError('No active Fusion design')
    addIn.log('execute', addIn.attributes)

    
    leadingEdge = addIn.attributes['leadingEdgePoint'][0]
    chordLine = addIn.attributes['chordLine'][0]

    # CHECK INPUT
    # Do it here before a new sketch is created.
    # Show error message and cancel the command if input is invalid.

    # The leading edge point input must be equal to one of these:
    startSketchPoint = chordLine.startSketchPoint
    endSketchPoint = chordLine.endSketchPoint

    # Special case when leading edge is the ORIGIN. The end end point of the line seems to be impossible 
    # to choose if that point is also the origin. 
    # If so, change leadingEdge to the chord line end point at (0,0).
    addIn.log('leadingEdge.geometry.x', leadingEdge.geometry.x)
    addIn.log('leadingEdge.geometry.y', leadingEdge.geometry.x)
    if(leadingEdge.geometry.x == 0 and leadingEdge.geometry.y == 0):
        addIn.log('leadingEdge is the ORIGIN')
        if(startSketchPoint.geometry.x == 0 and startSketchPoint.geometry.y == 0):
            addIn.log('Chord line start is also at (0,0))')
            leadingEdge = startSketchPoint
        elif(endSketchPoint.geometry.x == 0 and endSketchPoint.geometry.y == 0):
            addIn.log('Chord line end is also at (0,0)')
            leadingEdge = endSketchPoint
            
    # Check that the leading edge is a point on either end of the line
    addIn.log('leadingEdge == startSketchPoint', leadingEdge == startSketchPoint)
    addIn.log('leadingEdge == endSketchPoint', leadingEdge == endSketchPoint)

    if not (leadingEdge == startSketchPoint or leadingEdge == endSketchPoint):
        addIn.log('Wrong input! The choosen leading edge point is not an end point of the chord line', leadingEdge)
        utils.messageBox("The choosen leading edge point is not an end point of the chord line")
    else:
        # INPUT HAS BEEN CHECKED
        chordLineParentSketch = chordLine.parentSketch
        chordLineSketchPlane = chordLineParentSketch.referencePlane
        # Create a new sketch on the same plane as the chord line sketch
        sketches = addIn.rootComp.sketches
        airfoilSketch = sketches.add(chordLineSketchPlane)
        airfoilSketch.name = "Airfoil_" + str(round(chordLine.length * 10)) + "mm"
    
        # ceate airfoil
        airfoilData = airfoil.AirFoil(addIn.attributes['airfoilPath'])
        flipVertically = addIn.attributes['flipVertically']
        addIn.log('flipVertically', flipVertically)
        createAirfoilCurve(airfoilData, chordLine, leadingEdge, airfoilSketch, flipVertically)
   

# Creates an airfoil profile  curve on the given sketch (airfoilSketch).
# The airfoil profile is adjusted to the given chord line.
# The leading edge must be one of the end points of the chord line.
def createAirfoilCurve(airFoil, chordLine, leadingEdge, airfoilSketch, flipVertically):
    startSketchPoint = chordLine.startSketchPoint
    endSketchPoint = chordLine.endSketchPoint

    # Find the trailing edge
    trailingEdge = endSketchPoint
    if trailingEdge == leadingEdge:
        trailingEdge = chordLine.startSketchPoint

    leadingEdgeXOffset = leadingEdge.geometry.x
    leadingEdgeYOffset = leadingEdge.geometry.y
    trailingEdgeXOffset = trailingEdge.geometry.x
    trailingEdgeYOffset = trailingEdge.geometry.y
    deltaX = trailingEdgeXOffset - leadingEdgeXOffset
    deltaY = trailingEdgeYOffset - leadingEdgeYOffset

    # flipY = -1 negates the y coordinate.
    flipY = 1
    # Flip to compensate when x grows backwards, i.e the leading edge has a lesser x coordinate than the trailing edge.
    if (deltaX < 0):
        flipY = -1 * flipY

    # Flip if flipping is requested in the dialog box.
    if (flipVertically):
        flipY = -1 * flipY

    # Calculate airfoil points in the sketch coordinate system (which is in 2D, so x and y only, keep z = 0)
    # Airfoil points are transformed to be layed out along the chord line (the chord line is the x-axis)
    points = adsk.core.ObjectCollection.create()
    for p in list(airFoil.points):
        x = leadingEdgeXOffset + (deltaX * p.x) - (flipY * (deltaY * p.y))
        y = leadingEdgeYOffset + (flipY * (deltaX * p.y)) + (deltaY * p.x)
        point = adsk.core.Point3D.create(x, y, 0)
        points.add(point)

    # Create the airfoil curve on the sketch from the sketch points.
    airfoilSketch.sketchCurves.sketchFittedSplines.add(points)

    # Also add the chord line to the airfoil sketch
    # airfoilSketch.sketchCurves.sketchLines.addByTwoPoints(startSketchPoint.geometry, endSketchPoint.geometry)

params = [
    addin.SelectionParam('chordLine', 'SketchLines', 'Chord line of airfoil to import', 'click a sketch line to choose chord line', 1),
    addin.SelectionParam('leadingEdgePoint', 'SketchPoints', 'Leading edge point of airfoil to import', 'click a sketch point on the chord line to mark the leading edge', 1),
    addin.StringParam('airfoilPath', '', 'Airfoil Dat file path', 'Airfoil dat file path or URL'),
    addin.BoolParam('flipVertically', False, 'Flip wing vertically', 'Flip airfoil up side down')
]

addIn = addin.AddIn("wingProfileImporter", 'WingProfileImporter', params)
addIn.description = 'Imports an airfoil profile .dat file and adjusts it to a given chord line.'

addIn.executor = executor
addIn.fieldValidator = fieldValidator

def selChanged(args):
    pass
addIn.ui.activeSelectionChanged.add(addIn.handlers.make_handler(adsk.core.ActiveSelectionEventHandler, selChanged))

def run(context):
    try:
        addIn.addButton()
    except:
        utils.messageBox(traceback.format_exc())


def stop(context):
    try:
        addIn.removeButton()
    except:
        utils.messageBox(traceback.format_exc())
